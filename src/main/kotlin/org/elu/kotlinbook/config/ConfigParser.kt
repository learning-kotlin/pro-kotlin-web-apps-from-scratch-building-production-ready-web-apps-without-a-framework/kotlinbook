package org.elu.kotlinbook.config

import com.typesafe.config.ConfigFactory

fun createAppConfig(env: String) =
    ConfigFactory
        .parseResources("app-$env.conf")
        .withFallback(ConfigFactory.parseResources("app.conf"))
        .resolve()
        .let {
            WebAppConfig(
                httpPort = it.getInt("httpPort"),
                dbPassword = it.getString("dbPassword"),
            )
        }

package org.elu.kotlinbook.config

data class WebAppConfig(val httpPort: Int, val dbPassword: String?)

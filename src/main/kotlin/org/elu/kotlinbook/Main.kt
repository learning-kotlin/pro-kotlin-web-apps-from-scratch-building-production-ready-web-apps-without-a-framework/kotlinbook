package org.elu.kotlinbook

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.plugins.statuspages.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.elu.kotlinbook.config.WebAppConfig
import org.elu.kotlinbook.config.createAppConfig
import org.elu.kotlinbook.response.JsonWebResponse
import org.elu.kotlinbook.response.TextWebResponse
import org.elu.kotlinbook.response.webResponse
import org.slf4j.LoggerFactory
import kotlin.reflect.full.declaredMemberProperties

private val log = LoggerFactory.getLogger("org.elu.kotlinbook.Main")

fun main() {
    log.debug("Starting application...")

    val env = System.getenv("KOTLINBOOK_ENV") ?: "local"
    log.debug("Application runs in the environment $env")

    val config = createAppConfig(env)

    val secretsRegex = "password|secret|key".toRegex(RegexOption.IGNORE_CASE)
    log.debug("Configuration loaded successfully: \n${
        WebAppConfig::class.declaredMemberProperties
            .sortedBy { it.name }
            .joinToString(separator = "\n") {
                if (secretsRegex.containsMatchIn(it.name)) {
                    "\t${it.name} = ${it.get(config)?.toString()?.take(2)}*****"
                } else {
                    "\t${it.name} = ${it.get(config)}"
                }
            }
    }")

    embeddedServer(Netty, port = config.httpPort) {
        createKtorApplication()
    }.start(wait = true)
}

fun Application.createKtorApplication() {
    install(StatusPages) {
        exception<Throwable> { call, cause ->
            org.elu.kotlinbook.log.error("An unknown error occurred", cause)
            call.respondText(
                text = "500: $cause",
                status = HttpStatusCode.InternalServerError
            )
        }
    }
    routing {
        get("/original_ktor_root") {
            call.respondText("Hello, Ktor!")
        }
        get("/", webResponse {
            TextWebResponse("Hello, Kotlin!")
        })
        get("/param_test", webResponse {
            TextWebResponse("This param is ${call.request.queryParameters["foo"]}")
        })
        get("/json_test", webResponse {
            JsonWebResponse(mapOf("foo" to "bar"))
        })
        get("/json_test_with_header", webResponse {
            JsonWebResponse(mapOf("foo" to "bar"))
                .header("X-Test-Header", "Just a test!")
        })
    }
}

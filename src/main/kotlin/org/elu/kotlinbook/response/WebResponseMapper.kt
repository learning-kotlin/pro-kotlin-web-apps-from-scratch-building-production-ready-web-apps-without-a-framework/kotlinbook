package org.elu.kotlinbook.response

import com.google.gson.Gson
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.util.pipeline.*

fun webResponse(
    handler: suspend PipelineContext<Unit, ApplicationCall>.() -> WebResponse
): PipelineInterceptor<Unit, ApplicationCall> {
    return {
        val resp = this.handler()
        for ((name, values) in resp.headers()) {
            for (value in values) {
                call.response.header(name, value)
            }
        }
        val statusCode = HttpStatusCode.fromValue(resp.statusCode)
        when(resp) {
            is TextWebResponse -> {
                call.respondText(
                    text = resp.body,
                    status = statusCode,
                )
            }

            is JsonWebResponse -> {
                call.respond(KtorJsonWebResponse (
                    body = resp.body,
                    status = statusCode,
                ))
            }
        }
    }
}

class KtorJsonWebResponse (
    val body: Any?,
    override val status: HttpStatusCode = HttpStatusCode.OK
) : OutgoingContent.ByteArrayContent() {
    override val contentType = ContentType.Application.Json.withCharset(Charsets.UTF_8)
    override fun bytes() = Gson().toJson(body).toByteArray(Charsets.UTF_8)
}
